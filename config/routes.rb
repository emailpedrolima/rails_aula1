Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
get '/calculadora/:number1/:number2' , to: 'calculator#home'

get '/calculadora/soma/:number1/:number2', to: 'calculator#sum', as: 'soma'
get '/calculadora/:number1/:number2/subtracao', to: 'calculator#minus', as: 'subtracao'
get '/calculadora/:number1/:number2/divisao', to: 'calculator#division', as: 'divisao'
get '/calculadora/:number1/:number2/multiplicacao', to: 'calculator#multiply', as: 'multiplicacao'


end
